# author: zhangcsh5@outlook.com
# the script is for calculating Wind Speed Extrapolation

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from scipy.stats import pearsonr
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.datasets import make_regression
from scipy.optimize import curve_fit
from sklearn import datasets, linear_model, model_selection
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
plt.rcParams['font.sans-serif']=['SimHei'] #显示中文

def param_ws_shear(ws_mean, heights):         
    def func(x,b):
        return WS1_035_Mean0*x**b             
    popt, _ = curve_fit(func, heights, ws_mean, p0=0.1) #popt数组中，两个值分别是待求参数a,b                
    return popt

def Curve_Fitting(x,y,deg): 
    parameter = np.polyfit(x, y, deg) #拟合deg次多项式 
    p =np.poly1d(parameter) #拟合deg次多项式   
    aa=  ''
    #方程拼接
    if parameter[1]>=0:
        aa = '{:.3f}x+{:.3f}'.format(parameter[0],parameter[1])
    else:
        aa = '{:.3f}x{:.3f}'.format(parameter[0],parameter[1])
    '''
    for i in range(deg+1): 
        bb=round(parameter[i],2)
        if bb>0: 
            if i==0:
                bb=str(bb) 
            else: 
                bb='+'+str(bb) 
        else: 
            bb=str(bb)
        if deg==i: 
            aa=aa+bb
        else: 
            aa=aa+bb+'x^'+str(deg-i) #方程拼接一
            '''
    plt.plot(x,y,'.b',markersize=8) #原始数据散点图 
    plt.plot(x, p(x),color='g')# 画拟合曲线#
    #plt.text(-1,0,aa,fontdict={'size':'10'color:'b'}) 
    r = pearsonr(x,y)
    
    #plt.legend(['y={}'.format(aa),"$R^2=${:.4f}".format(r2_score(x,y))], prop = {'size':18},loc="upper center") #拼接好的方程和R方放到图例 
    #plt.legend(['y={}'.format(aa),"R$^2=${:.4f}".format(r[0]**2)], prop = {'size':18},loc="upper center") #拼接好的方程和R方放到图例 

if __name__ == '__main__':
    #fold_dir = 'D:/塔式雷达/机舱_UL/'
    fold_dir = os.getcwd()
    #print(fold_dir)
    file_name = "/Mast_2023-06-06_00-00-00_2024-01-26_00-00-00.csv"
    data_mast = pd.read_csv(fold_dir+file_name,sep=';')
    #columns = data_mast.columns
    #print(columns)
    '''
    columns = data_mast.columns
    for column in columns:
        if ("WS1" in column) and ("Mean" in column):
            #column = data_mast[column]
            print(column)
            '''
    WS1_035_Mean = data_mast['WS1_035_Mean']
    WS1_040_Mean = data_mast['WS1_040_Mean']
    WS1_060_Mean = data_mast['WS1_060_Mean']
    WS1_100_Mean = data_mast['WS1_100_Mean']
    WS1_136_Mean = data_mast['WS1_136_Mean']
    WS1_140_Mean = data_mast['WS1_140_Mean']
    
    #X = [WS1_035_Mean, WS1_040_Mean, WS1_060_Mean, WS1_100_Mean]
    #print(X)
    #y = [WS1_140_Mean]
    date = pd.to_datetime(data_mast['date'])
    #print(date)
    #date = pd.Timestamp.strftime(date, '%m/%d/%Y')
    time = pd.to_datetime(data_mast['date']+" "+data_mast['time'])

    #print(time)
    X_train = []
    y_train = []
    for i in range(20000):
        if WS1_035_Mean[i] >0.1 and WS1_040_Mean[i] >0.1 and WS1_060_Mean[i] >0.1 \
            and WS1_100_Mean[i] >0.1 and WS1_140_Mean[i] >0.1:
            X_train.append([WS1_035_Mean[i],WS1_040_Mean[i],WS1_060_Mean[i],WS1_100_Mean[i]]) 
            y_train.append(WS1_140_Mean[i])
    #X_train = np.array(X_train)
    #y_train = np.array(y_train)
    X_test = []
    y_test = []
    time_test = []
    y11 = []
    for j in np.arange(i,i+13000):
        if WS1_035_Mean[j] >0.1 and WS1_040_Mean[j] >0.1 and WS1_060_Mean[j] >0.1 \
            and WS1_100_Mean[j] >0.1 and WS1_140_Mean[j] >0.1:
            X_test.append([WS1_035_Mean[j],WS1_040_Mean[j],WS1_060_Mean[j],WS1_100_Mean[j]]) 
            y_test.append(WS1_140_Mean[j])
            y11.append(WS1_100_Mean[j])
            time_test.append(time[j])
    #X_test = np.array(X_test)
    #y_test = np.array(y_test)
    '''
    # 对比100m和140m风速的差异
    plt.figure()
    plt.plot(time_test,y_test)
    plt.plot(time_test,y11)
    plt.figure()
    plt.plot(y_test,y11,'.')
    print(r2_score(y_test,y11))
    plt.show()
    '''
    ################################################################
    # Solution 0
    # power law
    # Monin–Obukhov similarity theory
    ################################################################
    # Monin–Obukhov similarity theory 
    # 1. Calculate the Monin-Obukhov length
    # 2. Calculate the friction velocity
    # 3. Kinematic heat flux
    # 4. Air temperature @ 2m
    ################################################################
    WS1_035_Mean0 = np.mean(WS1_035_Mean)
    WS1_040_Mean0 = np.mean(WS1_040_Mean)
    WS1_060_Mean0 = np.mean(WS1_060_Mean)
    WS1_100_Mean0 = np.mean(WS1_100_Mean)
    WS1_136_Mean0 = np.mean(WS1_136_Mean)
    WS1_140_Mean0 = np.mean(WS1_140_Mean)

    height=[40/35,60/35,100/35,136/35,140/35]
    ws_mean = [WS1_040_Mean0, WS1_060_Mean0, WS1_100_Mean0, WS1_136_Mean0, WS1_140_Mean0]

    alpha = param_ws_shear(ws_mean, height)
    y_pred = []
    for j in range(len(X_test)):
        #print()
        y_140 = np.double(X_test[j][0])*(140/35)**alpha[0]
        y_pred.append(y_140)
    
    #print(y_pred)
    y_test0,y_pred0=[], []
    time0 = []
    for i in range(len(y_test)):
        if abs(y_test[i] - y_pred[i]) < 5 and y_pred[i] > 0:
            y_test0.append(y_test[i])
            y_pred0.append(y_pred[i])
            time0.append(time_test[i])
            #plt.scatter(y_test[i], y_test[i],  color='red')
    print(r2_score(y_test0, y_pred0))
    print('MAE = %.2f m/s'  %(mean_absolute_error(y_test0, y_pred0)))
    plt.figure()
    #print(y_test0)
    Curve_Fitting(y_test0, y_pred0, 1)
    plt.legend(["MAE = %.2f m/s" %(mean_absolute_error(y_test0, y_pred0))],fontsize = 30)
    #plt.plot(y_test1, y_pred1, '.b', markersize=8)
    # EN version
    #plt.xlabel('Regression estimated [m/s]',fontsize = 30)
    #plt.ylabel('Measured [m/s]',fontsize = 30)
    # CN version
    plt.xlabel('回归预测数据[m/s]',fontsize = 30)    
    plt.ylabel('测量数据[m/s]',fontsize = 30)
    plt.tick_params(labelsize=20)
    plt.figure()
    plt.plot(time0, y_test0, '.-', linewidth=3)
    plt.plot(time0, y_pred0, '.-', linewidth=3)

    plt.legend(['测量数据','回归预测数据'],loc='upper left',fontsize = 20)
    plt.xlabel('时间',fontsize = 30)
    plt.ylabel('风速 [m/s]',fontsize = 30)

    #plt.legend(['Measured','Regression estimated'],loc='upper left',fontsize = 20)
    #plt.xlabel('Time',fontsize = 30)
    #plt.ylabel('Wind speed [m/s]',fontsize = 30)
    
    plt.tick_params(labelsize=20)
    #plt.show()
    
    
    ################################################################
    # Solution 1
    # linear regression models
    ################################################################

    
    #from sklearn import linear_model
    #X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.33)
    model = linear_model.LinearRegression()
    #model = linear_model.PoissonRegressor()
    #model = linear_model.LogisticRegression()
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    #plt.scatter(X_test, y_test,  color='black')
    y_test1,y_pred1=[], []
    time1 = []
    for i in range(len(y_test)):
        if abs(y_test[i] - y_pred[i]) < 5 and y_pred[i] > 0:
            y_test1.append(y_test[i])
            y_pred1.append(y_pred[i])
            time1.append(time_test[i])
            #plt.scatter(y_test[i], y_test[i],  color='red')
    print(r2_score(y_test1, y_pred1))
    print('MAE = %.2f m/s'  %(mean_absolute_error(y_test1, y_pred1)))
    plt.figure()

    Curve_Fitting(y_test1, y_pred1, 1)
    plt.legend(["MAE = %.2f m/s" %(mean_absolute_error(y_test1, y_pred1))],fontsize = 30)
    #plt.plot(y_test1, y_pred1, '.b', markersize=8)
    # EN version
    #plt.xlabel('Regression estimated [m/s]',fontsize = 30)
    #plt.ylabel('Measured [m/s]',fontsize = 30)
    # CN version
    plt.xlabel('回归预测数据[m/s]',fontsize = 30)    
    plt.ylabel('测量数据[m/s]',fontsize = 30)
    plt.tick_params(labelsize=20)
    plt.figure()
    plt.plot(time1, y_test1, '.-', linewidth=3)
    plt.plot(time1, y_pred1, '.-', linewidth=3)
    
    plt.legend(['测量数据','回归预测数据'],loc='upper left',fontsize = 20)
    plt.xlabel('时间',fontsize = 30)
    plt.ylabel('风速 [m/s]',fontsize = 30)

    #plt.legend(['Measured','Regression estimated'],loc='upper left',fontsize = 20)
    #plt.xlabel('Time',fontsize = 30)
    #plt.ylabel('Wind speed [m/s]',fontsize = 30)
    plt.tick_params(labelsize=20)
    #plt.show()

    ################################################################
    # Solution 2
    # 多项式回归
    ################################################################    

    pipeline = make_pipeline(PolynomialFeatures(3), linear_model.LinearRegression())
    pipeline.fit(np.array(X_train), y_train)
    y_pred=pipeline.predict(X_test)
    time2 = []
    y_test2,y_pred2=[], []
    for i in range(len(y_test)):
        if abs(y_test[i] - y_pred[i]) < 5 and y_pred[i] > 0:
            y_test2.append(y_test[i])
            y_pred2.append(y_pred[i])
            time2.append(time_test[i])
    print(r2_score(y_test2, y_pred2))
    plt.figure()

    Curve_Fitting(y_test2, y_pred2, 1)
    plt.legend(["MAE = %.2f m/s" %(mean_absolute_error(y_test2, y_pred2))],fontsize = 30)
    print('MAE = %.2f m/s'  %(mean_absolute_error(y_test2, y_pred2)))
    #plt.plot(y_test1, y_pred1, '.b', markersize=8)
    # EN version
    #plt.xlabel('Regression estimated [m/s]',fontsize = 30)
    #plt.ylabel('Measured [m/s]',fontsize = 30)
    # CN version
    plt.xlabel('回归预测数据[m/s]',fontsize = 30)    
    plt.ylabel('测量数据[m/s]',fontsize = 30)
    plt.tick_params(labelsize=20)
    plt.figure()
    plt.plot(time2, y_test2, '.-', linewidth=3)
    plt.plot(time2, y_pred2, '.-', linewidth=3)

    plt.legend(['测量数据','回归预测数据'],loc='upper left',fontsize = 20)
    plt.xlabel('时间',fontsize = 30)
    plt.ylabel('风速 [m/s]',fontsize = 30)

    #plt.legend(['Measured','Regression estimated'],loc='upper left',fontsize = 20)
    #plt.xlabel('Time',fontsize = 30)
    #plt.ylabel('Wind speed [m/s]',fontsize = 30)
    plt.tick_params(labelsize=20)
    #plt.show()


    ################################################################
    # Solution 3
    # 随机森林回归器RandomForestRegressor
    ################################################################

    X, y = make_regression(n_features=4, n_informative=10,
                        random_state=0, shuffle=False)
    regr = RandomForestRegressor(max_depth=10, random_state=0)
    regr.fit(X_train, y_train)
    time3 = []
    y_test3,y_pred3=[], []
    y_pred = np.array(regr.predict(X_test))
    for i in range(len(y_test)):
        if abs(y_test[i] - y_pred[i]) < 5:
            y_test3.append(y_test[i])
            y_pred3.append(y_pred[i])
            time3.append(time_test[i])
    
    print(r2_score(y_test3, y_pred3))
    print('MAE = %.2f m/s'  %(mean_absolute_error(y_test3, y_pred3)))
    plt.figure()

    Curve_Fitting(y_test3, y_pred3, 1)
    plt.legend(["MAE = %.2f m/s" %(mean_absolute_error(y_test3, y_pred3))],fontsize = 30)
    #plt.plot(y_test1, y_pred1, '.b', markersize=8)
    # EN version
    #plt.xlabel('Regression estimated [m/s]',fontsize = 30)
    #plt.ylabel('Measured [m/s]',fontsize = 30)
    # CN version
    plt.xlabel('回归预测数据[m/s]',fontsize = 30)    
    plt.ylabel('测量数据[m/s]',fontsize = 30)
    plt.tick_params(labelsize=20)
    plt.figure()
    plt.plot(time3, y_test3, '.-', linewidth=3)
    plt.plot(time3, y_pred3, '.-', linewidth=3)

    plt.legend(['测量数据','回归预测数据'],loc='upper left',fontsize = 20)
    plt.xlabel('时间',fontsize = 30)
    plt.ylabel('风速 [m/s]',fontsize = 30)

    #plt.legend(['Measured','Regression estimated'],loc='upper left',fontsize = 20)
    #plt.xlabel('Time',fontsize = 30)
    #plt.ylabel('Wind speed [m/s]',fontsize = 30)
    plt.tick_params(labelsize=20)

    plt.show()
    
    '''
    # """  ############################################ 下面开始LSTM模型的部分
    # 创建一个序列模型
    import tensorflow as tf
    Sequential = tf.keras.models.Sequential
    model = Sequential()
    input_dim = 1  # 特征的数量
    time_steps = 65  # 多少个时间步为一组
    output_dim = 1  # 输出的维度，一组对应一个输出
    # 添加一个LSTM层，指定LSTM单元的数量和输入形状
    # model.add(tf.keras.layers.SimpleRNN(units=64, input_shape=(time_steps, input_dim)))
    LSTM = tf.keras.layers.LSTM
    model.add(LSTM(units=120, input_shape=(time_steps, input_dim)))

    # 添加一个全连接层，用于输出预测结果
    # model.add(Dense(units=5))
    Dense = tf.keras.layers.Dense
    model.add(Dense(units=output_dim, activation='linear'))

    # 编译模型 'adam'
    # model.compile(loss='mean_squared_error', optimizer=tf.keras.optimizers.Adam(learning_rate), metrics=['accuracy'])
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])  # loss参数是损失函数的类型，
    # optimizer参数是训练的方式，adam是一个自适应的方法比较简单，
    # metrics是显示某些信息，accuracy是精度，但本问题是回归问题，精度没有太多参考价值，一般是0

    # 打印模型摘要
    model.summary()
    normalized_data = np.array([X_train, y_train, X_test, y_test])
    # 训练模型
    #print(normalized_data)
    model.fit(X_train, y_train, epochs=120, batch_size=1000)

    # 评估模型
    pre_data = model.predict(X_test)

    '''

